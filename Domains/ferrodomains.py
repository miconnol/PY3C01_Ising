#!/usr/bin/env python
import numpy as np 
import scipy
import matplotlib.pylab as plt
import Ising 
import math
import sys

#simple script which plots spin config of antiferromagnet at a given temperature at eqm

n=30
run=2000
t=float(sys.argv[1])
lattice=Ising.lattice_random(n)
lattice=Ising.multi_sweep(lattice,t,run)
plt.title('Spin Configuration for a ferromagnet at t=%.3f'%t)
Ising.spin_plot(lattice)


