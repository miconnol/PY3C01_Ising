#!/bin/bash

#plots spin configurations for anti and ferromagnets for various temps, below tc/tn, around tc, around tn, and above tc/tn
for temp in 1.0 1.43 2.3 4.0 5.0; do
	./ferrodomains.py $temp;
	./antiferrodomains.py $temp;
done


