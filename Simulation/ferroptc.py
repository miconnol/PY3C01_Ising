#!/usr/bin/env python 

import numpy as np 
import scipy
import matplotlib.pylab as plt
import Ising 
import math

#observing behaviour of ferromagnet past critical temperature, just to check that it does just fluctuate around 0 and doesnt tend toward some other number
n=30
runtime=2000
temp=np.linspace(0.01,10.0,100)

lat=Ising.lattice_random(n)
mvals=[]
for i in range(runtime):
	lat=Ising.lattice_sweep(lat,5.0) 
	mvals.append(Ising.mag(lat))

print sum(mvals)/len(mvals) #average magnetisation

plt.plot(range(runtime),mvals)
plt.xlabel('Time steps')
plt.ylabel('Magnetisation')
plt.title('Evolution of magnetisation of a ferromagnet where $T>T_c$') #looks chaotic, but never deviates far from 0
plt.show()


