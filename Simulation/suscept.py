#!/usr/bin/env python 

import numpy as np 
import scipy
import matplotlib.pylab as plt
import Ising 
import math

n=30
#calculating magnetic susceptibility for a ferromagnet 
ts=np.linspace(0.5,4.0,30)
eqm_time=500 #time to send lattice to eqm
avgs=np.arange(0,1000,20) #time and no. of steps across which mag values will be taken 

#taking avgs across a larger range seems more important than having a long eqm time

sus=[] #magnetic susceptibility values 
i=1
for t in ts:
	print 'Running temperature %d/%d' %(i,len(ts)) #to give an idea of how long code is taking, because of the long runtime
	i=i+1

	mag=0
	mag_sqrd=0
	grid=Ising.lattice_random(n)
	grid=Ising.multi_sweep(grid,t,eqm_time)
	for avg in avgs:
		grid=Ising.multi_sweep(grid,t,avg)  #calculating magnetic susceptibility
		mag=mag+Ising.absmag(grid)
		mag_sqrd=mag_sqrd+(Ising.absmag(grid))**2
	h=(1/float(t))*((mag_sqrd/float(len(avgs)))-(mag/float(len(avgs)))**2)
	sus.append(math.fabs(h))
	
j=0
for val in sus:
	if val==np.amax(sus):
		tc=ts[j]
		print 'Critical temperature is %f with error %f' %(tc,ts[1]-ts[0]) #critical temperature must lie in this range, gives an idea of accuracy of computations
		break 
	j=j+1

plt.plot(ts,sus,'bo')
plt.xlabel('Temperature') 
plt.ylabel('Magnetic Susceptibility')
plt.title('Magnetic Susceptibility as a function of temperature, to identify $T_c$')
plt.show()
#provides a sample graph for magnetic susceptibility at J=1
#be warned, takes a long time to run but results are nice
