#!/usr/bin/env python 

import numpy as np 
import scipy
import matplotlib.pylab as plt
import Ising 
import math

#trying to identify a phase change in 1D
n=30
runtime=2000
temp=np.linspace(0.01,20.0,50)

#1D calculations, J=1
m=n**2 #ensure same amount of sites as nxn lattice
row=Ising.lattice_1D(m)
mags_1D=[]
for t in temp:
	line=Ising.multi_sweep_1D(row,t,runtime)
	ms=Ising.mag_avg_1D(line,t,100,10)
	mags_1D.append(ms) #same calculation for magnetisation values but in 1D

plt.plot(temp,mags_1D,'bo')
plt.xlabel('Temperature')
plt.ylabel('Magnetism')
plt.title('Magnetism vs Temperature of a 1D lattice to determine if a phase change occurs')
plt.show()

