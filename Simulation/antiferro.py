#!/usr/bin/env python 

import numpy as np 
import scipy
import matplotlib.pylab as plt
import Ising 
import math

#this script finds magnetism as a function of temperature for an antiferromanget

n=30 #lattice size
runtime=2000 #time each lattice runs for at each temp
temp=np.linspace(0.01,10.0,50) #range of temperature values

magarr=[]
lattice=Ising.lattice_random(n)
for t in temp:
	b=Ising.multi_sweep_J(lattice,t,runtime,-1.0) 
	d=Ising.mag_avg_J(b,t,200,10,-1.0)  #average over a few mag values
	magarr.append(d)
i=0
for mag in magarr:
	if mag>2/float(n*n): #provides a bit of tolerance for 2 sites breaking out of anti-parallel
		print 'Neel Temperature is:%.4f , Error:%.1f'%(temp[i],temp[1]-temp[0]) #identifying Neel temperature
		break
	i=i+1


plt.plot(temp,magarr,'bo')
plt.xlabel('Temperature')
plt.ylabel('Magnetisation')
plt.title('Magnetisation of an antiferromagnet')
plt.show()
 
