#!/bin/bash

#this script runs all files in the simulation directory
for file in ferro.py antiferro.py ferroptc.py suscept.py oned.py ; do
	echo "Running $file"
	./$file
	echo "Finished running $file"
done

