#!/usr/bin/env python 

import numpy as np 
import scipy
import matplotlib.pylab as plt
import Ising 
import math

#finding magnetism of ferromagnet at eqm at a range of temps
n=30
runtime=2000
temp=np.linspace(0.01,10.0,50)
magarr=[]

lattice=Ising.lattice_random(n)
for t in temp:
	a=Ising.multi_sweep(lattice,t,runtime)
	c=Ising.mag_avg(a,t,100,10) #takes each lattice to eqm, then gets an avg of magnetisation values once at eqm, to ensure not just one snapshot is taken but a good spread of values
	magarr.append(c)


plt.plot(temp,magarr,'bo')
plt.xlabel('Temperature')
plt.ylabel('Magnetisation')
plt.title('Magnetisation of a ferromagnet as a function of temperature')
plt.show()
