#!/usr/bin/env python

import numpy as np 
import matplotlib.pylab as plt
import math
import Ising
#not really part of my investigation, just a script i wrote and played around with to determine what sort of runtimes were needed to send lattices to eqm
n=20
temps=np.linspace(0.1,1,10)
counts=[]
counts1=[]

#establishing time taken to reach eqm for both collinear and random lattices at temps below critical 
for t in temps:
	grid=Ising.lattice_collinear(n)
	count=0
	while Ising.absmag(grid)<0.99:
		grid=Ising.lattice_sweep(grid,t)
		count=count+1
	counts1.append(count)
	print count

print counts1, 'collinear times'

for t in temps:
	grid=Ising.lattice_random(n)
	count=0
	while Ising.absmag(grid)<0.99:
		grid=Ising.lattice_sweep(grid,t)
		count=count+1
	counts.append(count)
	print count

print counts, 'random times'

#in most cases eqm is reached in less than 100 steps, however, sometimes takes alot longer 1000-100000 steps, happy medium between making sure eqm is reached but not outrageous computational time seems to be of the order of 1000-10000

run=1000
temp=0.5
lattice=Ising.lattice_random(n)
mags=[]
for r in range(run):
	mags.append(Ising.absmag(lattice))  
	lattice=Ising.lattice_sweep(lattice,temp)
	
plt.plot(range(0,run),mags)
plt.show() #once magnetisation reaches 1 it stays there, showing it is an eqm state
	







		
