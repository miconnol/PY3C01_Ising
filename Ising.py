#!/usr/bin/env python

import numpy as np
import matplotlib.pylab as plt
import math
import random

#module where i'll write functions that should be useful in the investigation

def lattice_collinear(n): #this function returns a numpy array of collinear ones and minus ones 
	arr=[]
	downs=-np.ones(n)
	ups=np.ones(n)
	i=0
	while i<n:
		arr.append(downs)
		arr.append(ups)
		i=i+2
	arr=np.array(arr) 
	return arr

def lattice_random(n): #randomly allocates 1 and -1 to a lattice of size n
	arr=[]
	a=1.0
	b=-1.0
	c=0.5
	for i in range(n):
		row=[]
		for j in range(n):
			if random.random()>c:
				row.append(a)
			else:
				row.append(b)
		arr.append(row)

	return np.array(arr)

	
def lattice_sweep(lattice,temp): #sweeps through random sites, with periodic boundary conditions implemented, J=1
	n=np.size(lattice[0])
	m=np.size(lattice)
	for i in range(m):	
		r=random.random()
		x=random.randint(0,n-1)
		y=random.randint(0,n-1)
		site=lattice[x][y]

		E_0=-site*(lattice[(x+1)%(n)][y]+lattice[(x-1)%(n)][y]+lattice[x][(y+1)%(n)]+lattice[x][(y-1)%(n)]) #modulo operation infinitely easier than writing 9 if statements
		
		delta_E=-2*E_0
		P_flip=np.exp(-delta_E/(temp))
		if delta_E<0:
			lattice[x][y]=-lattice[x][y]
		elif P_flip>r:
			lattice[x][y]=-lattice[x][y]
		
	return lattice

def lattice_sweep_J(lattice,temp,J): #performs lattice sweeps for abritrary J
	n=np.size(lattice[0])
	m=np.size(lattice)
	for i in range(m):	
		r=random.random()
		x=random.randint(0,n-1)
		y=random.randint(0,n-1)
		site=lattice[x][y]
	
		E_0=-J*site*(lattice[(x+1)%(n)][y]+lattice[(x-1)%(n)][y]+lattice[x][(y+1)%(n)]+lattice[x][(y-1)%(n)])

		delta_E=-2*E_0
		P_flip=np.exp(-delta_E/(temp))
		if delta_E<0:
			lattice[x][y]=-lattice[x][y]
		elif P_flip>r:
			lattice[x][y]=-lattice[x][y]
		
	return lattice

def multi_sweep_J(lattice,temp,run,J): #multiple sweeps for arbitrary J, might not use but handy to have
	i=0
	while i<run:
		lattice=lattice_sweep_J(lattice,temp,J)
		i=i+1
	return lattice

def multi_sweep(lattice,temp,run): #performs multiple sweeps with J=1
	i=0
	while i<run:
		lattice=lattice_sweep(lattice,temp)
		i=i+1
	return lattice

def netmag(lattice): #returns net magnetization of lattice
	mag=np.sum(lattice)
	return mag

def mag(lattice): #returns a value for magnetization between -1 and 1
	return float(netmag(lattice))/float(np.size(lattice))

def absmag(lattice): #returns value between 1 and -1
	return math.fabs(float(netmag(lattice)))/float(np.size(lattice))

def mag_avg(lattice,temp,run,n): #returns averaged magnetisation over a few values, ideally when lattice is at eqm
	runs=np.linspace(0,run,n)
	netmag=0
	for t in runs:
		lattice=multi_sweep(lattice,temp,t)
		netmag=netmag+absmag(lattice)
	return netmag/float(n)	

def mag_avg_J(lattice,temp,run,n,J):#same as above function but for arbtirary J
	runs=np.linspace(0,run,n)
	netmag=0
	for t in runs:
		a=multi_sweep_J(lattice,temp,t,J)
		netmag=netmag+absmag(lattice)
	return netmag/float(n)

def spin_plot(lattice): #plots the spin configuration of any given lattice
	plt.contourf(lattice,levels=[-1,0,1])
	plt.show()


#this contains functions concerned with computations in one dimensions, just a rehash of the 2d ones really, no significant differences

def lattice_1D(n):
	a=1.0
	b=-1.0
	c=0.5
	arr=[]
	for i in range(n):
		r=random.random()
		if r>c:
			arr.append(a)
		else:
			arr.append(b)
	return arr 

def sweep_1D(lattice,temp):
	n=np.size(lattice)
	for i in range(n):
		q=random.randint(0,n-1)
		site=lattice[q]
		E_0=-site*(lattice[(q+1)%n]+lattice[(q-1)%n])
		delta_E=-2*E_0
		P_flip=np.exp(-delta_E/(temp))
		if delta_E<0:
			lattice[q]=-lattice[q]
		elif random.random()>P_flip:
			lattice[q]=-lattice[q]
	return lattice

def multi_sweep_1D(lattice,temp,run):
	i=0
	while i<run:
		lattice=sweep_1D(lattice,temp)
		i=i+1
	return lattice

#magnetisation functions should be able to distinguish between 1d and 2d lattices
		
def mag_avg_1D(lattice,temp,run,n): #returns averaged magnetisation over a few values, ideally when lattice is at eqm
	runs=np.linspace(0,run,n)
	netmag=0
	for t in runs:
		lattice=multi_sweep_1D(lattice,temp,t)
		netmag=netmag+absmag(lattice)
	return netmag/float(n)	
	



	

















	
	
	
	
	














	
