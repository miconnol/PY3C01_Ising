# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
#ensuring I can access the Ising module

export PYTHONPATH="${PYTHONPATH}:/cphys/ugrad/2015-16/SF/MICONNOL/Ising_Model"
